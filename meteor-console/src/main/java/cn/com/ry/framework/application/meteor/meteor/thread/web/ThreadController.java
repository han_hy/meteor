package cn.com.ry.framework.application.meteor.meteor.thread.web;

import cn.com.ry.framework.application.meteor.framework.security.annotations.SecList;
import cn.com.ry.framework.application.meteor.framework.security.annotations.SecPrivilege;
import cn.com.ry.framework.application.meteor.framework.web.SpringControllerSupport;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/meteor/thread")
public class ThreadController extends SpringControllerSupport {

    @SecPrivilege(title = "线程管理")
    @SecList
    @RequestMapping(value = "/index")
    public String index(Model model) {
        String page = this.getViewPath("index");
        return page;
    }
}
