/****************************************************
 * Description: ServiceImpl for t_meteor_cmdlog
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author reywong
 * @version 1.0
 * @see
HISTORY
 *  2019-12-05 reywong Create File
 **************************************************/

package cn.com.ry.framework.application.meteor.meteor.websocket.service.impl;

import ch.ethz.ssh2.Connection;
import cn.com.ry.framework.application.meteor.framework.exception.ValidationException;
import cn.com.ry.framework.application.meteor.framework.ssh.SshTool;
import cn.com.ry.framework.application.meteor.framework.utils.StringUtils;
import cn.com.ry.framework.application.meteor.framework.web.support.XJJParameter;
import cn.com.ry.framework.application.meteor.meteor.machineinfo.entity.MachineinfoEntity;
import cn.com.ry.framework.application.meteor.meteor.machineinfo.service.MachineinfoService;
import cn.com.ry.framework.application.meteor.meteor.rsa.entity.RsaEntity;
import cn.com.ry.framework.application.meteor.meteor.rsa.service.RsaService;
import cn.com.ry.framework.application.meteor.meteor.websocket.service.WebsocketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WebsocketServiceImpl implements WebsocketService {
    @Autowired
    private MachineinfoService machineinfoService;
    @Autowired
    private RsaService rsaService;

    @Override
    public Connection getConnection(Long machineId) throws Exception {
        Connection connection = null;
        if (machineId != null) {
            if (machineId != null) {
                XJJParameter xjjParameter = new XJJParameter();
                xjjParameter.addQuery("query.id@eq@l", machineId);
                xjjParameter.addQuery("query.status@eq@s", "valid");
                List<MachineinfoEntity> machineinfoEntityList = machineinfoService.findList(xjjParameter);
                if (machineinfoEntityList != null && machineinfoEntityList.size() > 0) {
                    MachineinfoEntity machineinfoEntity = machineinfoEntityList.get(0);
                    String hostname = machineinfoEntity.getHostname();
                    if (StringUtils.isBlank(hostname)) {
                        throw new ValidationException("服务器地址为空");
                    }
                    String username = machineinfoEntity.getUsername();
                    String password = machineinfoEntity.getPassword();
                    Integer port = machineinfoEntity.getPort();
                    if (port == null) {
                        throw new ValidationException("连接端口为空");
                    }
                    if (machineinfoEntity.getLoginType() != null && machineinfoEntity.getLoginType().equals("rsa")) {
                        xjjParameter = new XJJParameter();
                        xjjParameter.addQuery("query.id@eq@l", machineinfoEntity.getRsaId().longValue());
                        xjjParameter.addQuery("query.status@eq@s", "valid");
                        List<RsaEntity> rsaEntityList = rsaService.findList(xjjParameter);
                        if (rsaEntityList != null && rsaEntityList.size() > 0) {
                            RsaEntity rsaEntity = rsaEntityList.get(0);
                            username = rsaEntity.getRsaUsername();
                            if (StringUtils.isBlank(username)) {
                                throw new ValidationException("用户名为空");
                            }
                            password = rsaEntity.getRsaPassword();
                            String rsaValue = rsaEntity.getRsaValue();
                            connection = SshTool.getConnection(hostname, username, port, rsaValue.toCharArray(), password);
                        } else {
                            throw new ValidationException("该服务器不存在，请检查是否禁用");
                        }
                    } else {
                        if (StringUtils.isBlank(username)) {
                            throw new ValidationException("用户名为空");
                        }
                        if (StringUtils.isBlank(password)) {
                            throw new ValidationException("密码为空");
                        }
                        connection = SshTool.getConnection(hostname, username, password, port);
                    }
                } else {
                    throw new ValidationException("该服务器不存在，请检查是否禁用");
                }
            } else {
                throw new ValidationException("服务器ID不能为空");
            }
        }
        return connection;
    }
}
