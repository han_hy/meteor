/****************************************************
 * Description: DAO for 服务器信息
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author reywong
 * @version 1.0
 * @see
HISTORY
 *  2019-12-04 reywong Create File
 **************************************************/
package cn.com.ry.framework.application.meteor.meteor.machineinfo.dao;

import cn.com.ry.framework.application.meteor.framework.dao.XjjDAO;
import cn.com.ry.framework.application.meteor.meteor.machineinfo.entity.MachineinfoEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface MachineinfoDao extends XjjDAO<MachineinfoEntity> {
    List<String> findModuleList(@Param("userId") Long userId);

    List<MachineinfoEntity> findHostListByModule(@Param("userId") Long userId, @Param("moduleName") String moduleName);

    MachineinfoEntity findMachineByMachineId(@Param("userId") Long userId, @Param("machineId") Long machineId, @Param("moduleName") String moduleName);
}

