/****************************************************
 * Description: DAO for 私钥信息
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author      reywong
 * @version     1.0
 * @see
	HISTORY
    *  2019-12-13 reywong Create File
**************************************************/
package cn.com.ry.framework.application.meteor.meteor.rsa.dao;

import cn.com.ry.framework.application.meteor.meteor.rsa.entity.RsaEntity;
import cn.com.ry.framework.application.meteor.framework.dao.XjjDAO;


public interface RsaDao  extends XjjDAO<RsaEntity> {

}

