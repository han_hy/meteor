/****************************************************
 * Description: ServiceImpl for t_sec_role_privilege
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      RY
 * @version     1.0
 * @see
	HISTORY
    *  2018-04-18 RY Create File
**************************************************/

package cn.com.ry.framework.application.meteor.sec.service.impl;

import cn.com.ry.framework.application.meteor.framework.dao.XjjDAO;
import cn.com.ry.framework.application.meteor.sec.dao.RoleDao;
import cn.com.ry.framework.application.meteor.sec.dao.RolePrivilegeDao;
import cn.com.ry.framework.application.meteor.sec.entity.RoleEntity;
import cn.com.ry.framework.application.meteor.sec.entity.RolePrivilegeEntity;
import cn.com.ry.framework.application.meteor.sec.service.RolePrivilegeService;
import cn.com.ry.framework.application.meteor.framework.security.PrivilegeService;
import cn.com.ry.framework.application.meteor.framework.security.dto.Function;
import cn.com.ry.framework.application.meteor.framework.security.dto.Privilege;
import cn.com.ry.framework.application.meteor.framework.security.dto.TreeNode;
import cn.com.ry.framework.application.meteor.framework.service.XjjServiceSupport;
import cn.com.ry.framework.application.meteor.framework.utils.StringUtils;
import cn.com.ry.framework.application.meteor.framework.web.support.XJJParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class RolePrivilegeServiceImpl extends XjjServiceSupport<RolePrivilegeEntity> implements RolePrivilegeService {

	@Autowired
	private RolePrivilegeDao rolePrivilegeDao;

	@Autowired
	private RoleDao roleDao;

	@Override
	public XjjDAO<RolePrivilegeEntity> getDao() {

		return rolePrivilegeDao;
	}
	public RolePrivilegeEntity getByParam(XJJParameter param)
	{
		List<RolePrivilegeEntity> list = rolePrivilegeDao.findList(param.getQueryMap());

		if(list.size()==1)
		{
			return list.get(0);
		}
		return null;
	}

	public List<TreeNode> listpri(Long roleid) {
		// 1.得到角色
		RoleEntity role = roleDao.getById(roleid);

		// root
		List<TreeNode> list = new ArrayList<TreeNode>();
		TreeNode root = new TreeNode();
		root.setText(role.getTitle());
		root.setState("open");
		root.setId("rootNode");
		root.setNodes(ptoTree(findPrivileges(), roleid));

		list.add(root);

		return list;
	}

	private Collection<Privilege> findPrivileges() {
		return PrivilegeService.getPrivileges();
	}

	private List<TreeNode> ptoTree(Collection<Privilege> privileges, Long roleid) {
		List<TreeNode> list = new ArrayList<TreeNode>();
		if (privileges == null)
			return list;

		TreeNode node;
		for (Privilege p : privileges) {
			node = new TreeNode();
			node.setText(p.getTitle());
			node.setId(p.getCode());
			Map<String,Object> map = ftoTree(p.getCode(), roleid);
			node.setNodes((List<TreeNode>) map.get("list"));
			node.setChecked(map.get("flag")==null?false:((Boolean)map.get("flag")));
			list.add(node);
		}

		return list;
	}

	private Map<String,Object> ftoTree(String pcode, Long roleid) {
		Map<String,Object> map = new HashMap<String, Object>();
		List<TreeNode> list = new ArrayList<TreeNode>();

		Privilege privilege = PrivilegeService.getPrivilege(pcode);
		if (privilege == null)
			return map;
		Collection<Function> functions = privilege.getFunctions();
		if (functions == null)
			return map;
		List<String> funs = new ArrayList<String>();
		RolePrivilegeEntity rolePrivilege = rolePrivilegeDao.getByRolePri(roleid,
				pcode);
		if (rolePrivilege != null)
			funs = rolePrivilege.getFunctions();

		TreeNode node;
		Integer num = 0;
		for (Function f : functions) {
				if(StringUtils.isBlank(f.getTitle())){
					continue;
				}
				node = new TreeNode();
				node.setText(f.getTitle());
				node.setId(f.getCode());
				if (checkFun(f, funs)){
					node.setChecked(true);
					num++;
				}
				list.add(node);
		}
		if(num != 0){
			map.put("flag", true);
		}else{
			map.put("flag", false);
		}
		map.put("list", list);
		return map;
	}

	private boolean checkFun(Function f, List<String> funs) {
		if (f.getCode().equals("default"))
			return true;
		for (String s : funs) {
			if (s.equals(f.getCode()))
				return true;
		}

		return false;
	}
}
