/****************************************************
 * Description: Entity for 数据字典
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author      reywong
 * @version     1.0
 * @see
	HISTORY
    *  2019-08-16 reywong Create File
**************************************************/

package cn.com.ry.framework.application.meteor.sys.dict.entity;

import cn.com.ry.framework.application.meteor.framework.entity.EntitySupport;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class DictEntity extends EntitySupport {

    private static final long serialVersionUID = 1L;
    public DictEntity(){}
    private String code;//编码
    private String detail;//备注
    private String groupCode;//属组
    private String groupName;
    private String name;//名称
    private Integer sn;//序号
    private String status;//状态
    /**
     * 返回编码
     * @return 编码
     */
    public String getCode() {
        return code;
    }

    /**
     * 设置编码
     * @param code 编码
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 返回备注
     * @return 备注
     */
    public String getDetail() {
        return detail;
    }

    /**
     * 设置备注
     * @param detail 备注
     */
    public void setDetail(String detail) {
        this.detail = detail;
    }

    /**
     * 返回属组
     * @return 属组
     */
    public String getGroupCode() {
        return groupCode;
    }

    /**
     * 设置属组
     * @param groupCode 属组
     */
    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    /**
     * 返回名称
     * @return 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置名称
     * @param name 名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 返回序号
     * @return 序号
     */
    public Integer getSn() {
        return sn;
    }

    /**
     * 设置序号
     * @param sn 序号
     */
    public void setSn(Integer sn) {
        this.sn = sn;
    }

    /**
     * 返回状态
     * @return 状态
     */
    public String getStatus() {
        return status;
    }

    /**
     * 设置状态
     * @param status 状态
     */
    public void setStatus(String status) {
        this.status = status;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).append("cn.com.ry.framework.application.oil.sys.dict.entity.DictEntity").append("ID="+this.getId()).toString();
    }
}

