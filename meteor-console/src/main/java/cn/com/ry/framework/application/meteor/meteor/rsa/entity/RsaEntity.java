/****************************************************
 * Description: Entity for 私钥信息
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author reywong
 * @version 1.0
 * @see
HISTORY
 *  2019-12-13 reywong Create File
 **************************************************/

package cn.com.ry.framework.application.meteor.meteor.rsa.entity;

import cn.com.ry.framework.application.meteor.framework.entity.EntitySupport;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class RsaEntity extends EntitySupport {

    private static final long serialVersionUID = 1L;

    public RsaEntity() {
    }

    private String rsaName;//私钥名称
    private String rsaValue;//私钥内容
    private String rsaUsername;//私钥用户
    private String rsaPassword;//私钥密码
    private String status;//状态
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;//创建时间
    private String createPersonId;//创建人员ID
    private String createPersonName;//创建人员姓名

    /**
     * 返回私钥名称
     *
     * @return 私钥名称
     */
    public String getRsaName() {
        return rsaName;
    }

    /**
     * 设置私钥名称
     *
     * @param rsaName 私钥名称
     */
    public void setRsaName(String rsaName) {
        this.rsaName = rsaName;
    }

    /**
     * 返回私钥内容
     *
     * @return 私钥内容
     */
    public String getRsaValue() {
        return rsaValue;
    }

    /**
     * 设置私钥内容
     *
     * @param rsaValue 私钥内容
     */
    public void setRsaValue(String rsaValue) {
        this.rsaValue = rsaValue;
    }

    /**
     * 返回私钥用户
     *
     * @return 私钥用户
     */
    public String getRsaUsername() {
        return rsaUsername;
    }

    /**
     * 设置私钥用户
     *
     * @param rsaUsername 私钥用户
     */
    public void setRsaUsername(String rsaUsername) {
        this.rsaUsername = rsaUsername;
    }

    /**
     * 返回私钥密码
     *
     * @return 私钥密码
     */
    public String getRsaPassword() {
        return rsaPassword;
    }

    /**
     * 设置私钥密码
     *
     * @param rsaPassword 私钥密码
     */
    public void setRsaPassword(String rsaPassword) {
        this.rsaPassword = rsaPassword;
    }

    /**
     * 返回状态
     *
     * @return 状态
     */
    public String getStatus() {
        return status;
    }

    /**
     * 设置状态
     *
     * @param status 状态
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 返回创建时间
     *
     * @return 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 返回创建人员ID
     *
     * @return 创建人员ID
     */
    public String getCreatePersonId() {
        return createPersonId;
    }

    /**
     * 设置创建人员ID
     *
     * @param createPersonId 创建人员ID
     */
    public void setCreatePersonId(String createPersonId) {
        this.createPersonId = createPersonId;
    }

    /**
     * 返回创建人员姓名
     *
     * @return 创建人员姓名
     */
    public String getCreatePersonName() {
        return createPersonName;
    }

    /**
     * 设置创建人员姓名
     *
     * @param createPersonName 创建人员姓名
     */
    public void setCreatePersonName(String createPersonName) {
        this.createPersonName = createPersonName;
    }


    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).append("cn.com.ry.framework.application.meteor.meteor.rsa.entity.RsaEntity").append("ID=" + this.getId()).toString();
    }
}

