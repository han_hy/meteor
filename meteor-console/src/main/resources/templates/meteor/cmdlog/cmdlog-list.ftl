<#--
/****************************************************
 * Description: t_meteor_cmdlog的简单列表页面，没有编辑功能
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author      reywong
 * @version     1.0
 * @see
	HISTORY
    *  2019-12-05 reywong Create File
**************************************************/
-->
<#include "/templates/xjj-list.ftl"> 
<@list id=tabId>
	<thead>
		<tr>
			<th><input type="checkbox" class="bscheckall"></th>
	        <th>服务器地址</th>
	        <th>应用名称</th>
	        <th>命令</th>
	        <th>操作人员ID</th>
	        <th>操作人员名称</th>
	        <th>创建时间</th>
	        <th>操作</th>
		</tr>
	</thead>
	<tbody>
		<#list page.items?if_exists as item>
		<tr>
			<td>
			<input type="checkbox" class="bscheck" data="id:${item.id}">
			</td>
			<td>
							${item.hostname}

			</td>
			<td>
							${item.appname}

			</td>
			<td>
							${item.cmd}

			</td>
			<td>
							${item.createPersonId}

			</td>
			<td>
							${item.createPersonName}

			</td>
			<td>
			    ${item.createTime?string('yyyy-MM-dd HH:mm:ss')}
			</td>
			<td>
            	<@button type="purple" icon="fa fa-pencil" onclick="XJJ.edit('${base}/meteor/cmdlog/input/${item.id}','修改t_meteor_cmdlog','${tabId}');">修改</@button>
				<@button type="danger" icon=" fa fa-trash-o" onclick="XJJ.del('${base}/meteor/cmdlog/delete/${item.id}','删除t_meteor_cmdlog？',false,{id:'${tabId}'});">删除</@button>
            </td>
		</tr>
		</#list>
	</tbody>
</@list>