<#--
/****************************************************
 * Description: t_meteor_cmdlog的列表页面
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author      reywong
 * @version     1.0
 * @see
	HISTORY
    *  2019-12-05 reywong Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl">
<@navList navs=navArr/>
<@content id=tabId>
	<@query queryUrl="${base}/meteor/cmdlog/list" id=tabId>
		<@querygroup title='名称'>
			<input type="search" name="query.name@lk@s" class="form-control input-sm" placeholder="请输入名称" aria-controls="dynamic-table">
	    </@querygroup>
		<@querygroup title='状态'>
			<@select name="query.status@eq@s" list=XJJConstants.COMMON_STATUS_LIST></@select>
	    </@querygroup>
		<@button type="info" icon="glyphicon glyphicon-search" onclick="XJJ.query({id:'${tabId}'});">查询</@button>
	</@query>


	<@button type="info" icon="glyphicon glyphicon-plus" onclick="XJJ.add('${base}/meteor/cmdlog/input','添加t_meteor_cmdlog','${tabId}');">增加</@button>
	<@button type="purple" icon="fa fa-pencil" onclick="XJJ.edit('${base}/meteor/cmdlog/input','修改t_meteor_cmdlog','${tabId}');">修改</@button>
	<@button type="danger" icon=" fa fa-trash-o" onclick="XJJ.del('${base}/meteor/cmdlog/delete','删除t_meteor_cmdlog？',true,{id:'${tabId}'});">删除</@button>

	<@button type="success" icon="fa fa-cloud-upload" onclick="XJJ.add('${base}/meteor/cmdlog/import','导入','${tabId}');">导入</@button>
	<@button type="success" icon="fa fa-cloud-download" onclick="window.location.href='${base}/meteor/cmdlog/export/excel';">导出</@button>

</@content>
