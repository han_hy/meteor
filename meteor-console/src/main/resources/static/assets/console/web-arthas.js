
var ws;
var xterm;

/** begin connect **/
function startconnect() {
    var ip = document.getElementById("ip").value;
    var port = document.getElementById("port").value;
    var agentId = document.getElementById("agentId").value;
    if (ip == '' || port == '') {
        alert('Ip or port can not be empty');
        return;
    }
    if (ws != null) {
        disconnect();
        alert('Already connected');
    }

    var path = 'ws://' + ip + ':' + port + '/ws?method=connectArthas&id=' + agentId;
    if (agentId == '' || agentId == undefined || agentId == null) {
        path = 'ws://' + ip + ':' + port + '/ws';
    }
    ws = new WebSocket(path);

    ws.onerror = function () {
        ws = null;
        alert('Connect error');
    };
    ws.onclose = function (message) {
        if (message.code === 2000) {
            alert(message.reason);
        }
    };
    ws.onopen = function () {
        console.log('open');
        $('#fullSc').show();

        xterm = initXterm("terminal-card");
        ws.onmessage = function (event) {
            if (event.type === 'message') {
                data = event.data;
                if (xterm != null) {
                    xterm.write(data);
                }
            }
        };

        xterm.open(document.getElementById('terminal'));

        xterm.on('data', function (data) {
            ws.send(JSON.stringify({action: 'read', data: data}))
            console.log(JSON.stringify({action: 'read', data: data}));
        });
        var terminalSize = getTerminalSize("terminal-card");
        ws.send(JSON.stringify({action: 'resize', cols: terminalSize.cols, rows: terminalSize.rows}));

        window.setInterval(function () {
            if (ws != null) {
                ws.send(JSON.stringify({action: 'read', data: ""}));
            }
        }, 30000);
    }
}

function disconnect() {
    try {
        ws.onmessage = null;
        ws.onclose = null;
        ws = null;
        xterm.destroy();
        $('#fullSc').hide();
        alert('Connection was closed successfully!');
    } catch (e) {
        alert('No connection, please start connect first.');
    }
}

/** full screen show **/
function xtermFullScreen() {
    var ele = document.getElementById('terminal-card');
    requestFullScreen(ele);
}

function requestFullScreen(element) {
    var requestMethod = element.requestFullScreen || element.webkitRequestFullScreen || element.mozRequestFullScreen || element.msRequestFullScreen;
    if (requestMethod) {
        requestMethod.call(element);
    } else if (typeof window.ActiveXObject !== "undefined") {
        var wscript = new ActiveXObject("WScript.Shell");
        if (wscript !== null) {
            wscript.SendKeys("{F11}");
        }
    }
}

window.addEventListener('resize', function () {
    var terminalSize = getTerminalSize("terminal-card");
    ws.send(JSON.stringify({action: 'resize', cols: terminalSize.cols, rows: terminalSize.rows}));
    xterm.resize(terminalSize.cols, terminalSize.rows);
});
